# README integration-quickstart#

This project helps you create a Maven archetype which consists of all Spring boot Application

#use the below command to deploy on artifactory
clean compile deploy

#use the below command to generate project from archetype
archetype:create-from-project -B -DarchetypeArtifactId=maven-archetype-archetype -DgroupId=com.zycus.integration.platform -DartifactId=integration-quickstart -Dversion=1.0.0-SNAPSHOT

#use the below command to install on local
clean compile install
