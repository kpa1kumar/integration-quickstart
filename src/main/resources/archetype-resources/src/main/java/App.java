#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.tanukisoftware.wrapper.WrapperListener;
import org.tanukisoftware.wrapper.WrapperManager;

@SpringBootApplication
public class App implements WrapperListener {
    private ConfigurableApplicationContext applicationContext;
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    public void controlEvent(int event) {
        LOG.info("Received controlEvent:{}", Integer.valueOf(event));
        if (WrapperManager.isControlledByNativeWrapper()) {
            LOG.info("Skipped processing controlEvent:{} as managed by {}", Integer.valueOf(event),
                    WrapperManager.class.getName());
        } else if ((event == 200) || (event == 201) || (event == 203)) {
            LOG.info("Processing controlEvent:{}", Integer.valueOf(event));
            WrapperManager.stop(0);
        }
    }

    public Integer start(String[] args) {
        try {
            LOG.info("Starting application");
            SpringApplicationBuilder appBuilder = new SpringApplicationBuilder(new Object[] { App.class });
            appBuilder.run(args);

            LOG.info("Application started");
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            return Integer.valueOf(-1);
        }
        return null;
    }

    public int stop(int code) {
        this.applicationContext.stop();
        this.applicationContext.close();
        return 0;
    }

    public static void main(String[] args) {
        WrapperManager.start(new App(), args);
    }
}