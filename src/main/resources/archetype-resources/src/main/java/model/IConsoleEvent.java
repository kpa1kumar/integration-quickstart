#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ICONSOLE_EVENT_DATA")
public class IConsoleEvent {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long   id;

    @Column(name = "TENANT_ID", length = 50)
    private String tenantId;

    @Column(name = "EVENT_ID", length = 255)
    private String eventId;

    @Column(name = "ENTITY_ID", length = 255)
    private String entityId;

    @Column(name = "ENTITY_TYPE", length = 255)
    private String entityType;

    @Column(name = "EVENT_TYPE", length = 255)
    private String eventType;

    @Column(name = "VERSION")
    private int    version;

    @Column(name = "CREATED_ON")
    private Date   createdOn;

    @Column(name = "DELIVERY_STATUS", length = 255)
    private int    deliveryStatus;

    @Column(name = "SOURCE_SYSTEM", length = 255)
    private String sourceSystem;

    @Column(name = "MODIFIED_ON")
    private Date   modifiedOn;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTenantId() {
        return this.tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getEntityId() {
        return this.entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityType() {
        return this.entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public String getEventType() {
        return this.eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public int getVersion() {
        return this.version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public int getDeliveryStatus() {
        return this.deliveryStatus;
    }

    public void setDeliveryStatus(int deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getSourceSystem() {
        return this.sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    public Date getModifiedOn() {
        return this.modifiedOn;
    }

    public void setModifiedOn(Date modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String toString() {
        return "IConsoleEvent [id=" + this.id + ", tenantId=" + this.tenantId + ", eventId=" + this.eventId + ", entityId=" + this.entityId + ", entityType=" + this.entityType + ", eventType="
                + this.eventType + ", version=" + this.version + ", createdOn=" + this.createdOn + ", deliveryStatus=" + this.deliveryStatus + ", sourceSystem=" + this.sourceSystem + ", modifiedOn="
                + this.modifiedOn + "]";
    }
}