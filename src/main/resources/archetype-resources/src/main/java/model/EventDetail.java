#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "EVENT_DETAIL")
public class EventDetail implements Serializable {
    private static final long serialVersionUID = 3966784100283875090L;
    private Long              id;
    private Date              receivedTimestamp;
    private String            sourceProduct;
    private String            eventJson;
    private IConsoleEvent     iConsoleEvent;

    public EventDetail() {
    }

    public EventDetail(Date receivedTimestamp, String sourceProduct, IConsoleEvent iConsoleEvent, String eventJson) {
        this.receivedTimestamp = receivedTimestamp;
        this.sourceProduct = sourceProduct;
        this.iConsoleEvent = iConsoleEvent;
        this.eventJson = eventJson;
    }

    @Id
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "RECEIVED_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getReceivedTimestamp() {
        return this.receivedTimestamp;
    }

    public void setReceivedTimestamp(Date receivedTimestamp) {
        this.receivedTimestamp = receivedTimestamp;
    }

    @Column(name = "SOURCE_PRODUCT ")
    public String getSourceProduct() {
        return this.sourceProduct;
    }

    public void setSourceProduct(String sourceProduct) {
        this.sourceProduct = sourceProduct;
    }

    @OneToOne(fetch = FetchType.EAGER)
    @MapsId
    public IConsoleEvent getiConsoleEvent() {
        return this.iConsoleEvent;
    }

    public void setiConsoleEvent(IConsoleEvent iConsoleEvent) {
        this.iConsoleEvent = iConsoleEvent;
    }

    @Column(name = "EVENT_JSON", length = 4000)
    public String getEventJson() {
        return this.eventJson;
    }

    public void setEventJson(String eventJson) {
        this.eventJson = eventJson;
    }

    public String toString() {
        return "EventDetail [id=" + this.id + ", receivedTimestamp=" + this.receivedTimestamp + ", sourceProduct=" + this.sourceProduct + ", eventJson=" + this.eventJson + ", iConsoleEvent="
                + this.iConsoleEvent + "]";
    }
}