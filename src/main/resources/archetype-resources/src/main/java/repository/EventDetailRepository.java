#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ${package}.model.EventDetail;

@Repository
public abstract interface EventDetailRepository extends JpaRepository<EventDetail, Long> {
    @Query("select e from EventDetail e where e.receivedTimestamp >= :from and e.receivedTimestamp <= :to and e.sourceProduct = :sourceProduct order by e.receivedTimestamp desc")
    public abstract List<EventDetail> findEventsBetweenWithProduct(@Param("from") Date paramDate1, @Param("to") Date paramDate2, @Param("sourceProduct") String paramString);

    @Query("select e from EventDetail e where e.receivedTimestamp >= :from and e.receivedTimestamp <= :to order by e.receivedTimestamp desc")
    public abstract List<EventDetail> findEventsBetween(@Param("from") Date paramDate1, @Param("to") Date paramDate2);
}